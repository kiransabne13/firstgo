package config

import (
	"database/sql"
	"fmt"

	_ "github.com/lib/pq"
)

var Db *sql.DB

func InitDB() {
	var err error
	//psqlInfo := fmt.Sprintf("host=%s port=%s, user=%s password=%s dbname=%s sslmode=disable", config[dbhost], config[dbport], config[dbuser], config[dbpass], config[dbname])

	connStr := "postgres://kiran:newpassword@localhost/hrms_app?sslmode=disable"

	database, err := sql.Open("postgres", connStr)
	if err != nil {
		panic(err)
	}

	err = database.Ping()
	if err != nil {
		panic(err)
	}

	Db = database

	fmt.Println("Successfully connected!")
}
