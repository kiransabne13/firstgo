package main

import (
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/kiransabne/gofirstapp/config"

	"github.com/gorilla/mux"
	_ "github.com/lib/pq"
)

const (
	dbhost = "127.0.0.1"
	dbport = "5432"
	dbuser = "kiran"
	dbpass = "newpassword"
	dbname = "hrms_app"
)

func main() {
	fmt.Printf("jdfkjdbskjsg00\n")
	config.InitDB()
	//InitDB()
	// db, err := sql.Open("postgres", "dbname=hrms_app sslmode=disable")
	// if err != nil {
	// 	panic(err)
	// }

	router := mux.NewRouter()

	port := os.Getenv("PORT") //Get port from .env file, we did not specify any port so this should return an empty string when tested locally
	if port == "" {
		port = "8000" //localhost
	}

	fmt.Println(port)

	router.HandleFunc("/", testHandler)

	err := http.ListenAndServe(":"+port, router) //Launch the app, visit localhost:8000/api
	if err != nil {
		fmt.Print(err)
	}

	//defer db.Close()
	defer config.Db.Close()
	log.Println("started")
	fmt.Println("started")
}

func testHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, string("out"))
}
